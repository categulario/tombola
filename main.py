#!/usr/bin/env python3
import curses
import sys
from curses import wrapper
from random import randint, choice

FACTOR = 5

def tombola(stdscr, selected_word):
    # Clear screen
    stdscr.clear()

    to_paint = ((curses.COLS * curses.LINES) // 5) // FACTOR

    for i in range(to_paint):
        stdscr.addstr(randint(0, curses.LINES-1), randint(0, curses.COLS-1), "?")
        curses.napms(7 * FACTOR)
        stdscr.refresh()

    stdscr.getkey()

    # reveal the word
    slen = len(selected_word)
    word_x = curses.COLS//2 - slen//2
    word_y = curses.LINES//2

    # top border
    stdscr.addstr(word_y-2, word_x-2, '+')
    stdscr.addstr(word_y-2, word_x-1, '-'*(slen + 2))
    stdscr.addstr(word_y-2, word_x + slen + 1, '+')

    # clear top space
    stdscr.addstr(word_y-1, word_x-1, ' '*(slen + 2))
    stdscr.addstr(word_y+1, word_x-1, ' '*(slen + 2))
    stdscr.addstr(word_y, word_x-1, ' ')
    stdscr.addstr(word_y, word_x + slen, ' ')

    # vertical borders
    for i in range(3):
        stdscr.addstr(word_y - 1 + i, word_x - 2, '|')
        stdscr.addstr(word_y - 1 + i, word_x + slen + 1, '|')

    # bottom border
    stdscr.addstr(word_y+2, word_x-2, '+')
    stdscr.addstr(word_y+2, word_x-1, '-'*(slen + 2))
    stdscr.addstr(word_y+2, word_x + slen + 1, '+')

    stdscr.addstr(word_y, word_x, selected_word)

    stdscr.getkey()


def main(filename):
    with open(filename) as textfile:
        words = list(filter(lambda x: x, map(lambda line: line.strip(), textfile)))

        wrapper(tombola, choice(words))


if __name__ == '__main__':
    try :
        main(sys.argv[1])
    except IndexError:
        print('needs an input file')
